import './App.css'
import World from './components/World'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        Hello
      </header>
      <World />
    </div>
  )
}

export default App
